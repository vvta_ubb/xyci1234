package maxScoreCounter.domain;

public enum Difficulty {
    Easy, Medium, Hard, None
}
