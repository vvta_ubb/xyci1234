package maxScoreCounter.repository;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import maxScoreCounter.domain.Difficulty;
import maxScoreCounter.domain.Quiz;
import maxScoreCounter.validation.QuizValidator;


public class Repository {
	

	private Vector<Quiz> quizzes;
	private String fileName;

	public QuizValidator getValidator() {
		return validator;
	}

	private QuizValidator validator;

	public Repository(String fileName, QuizValidator validator) throws IOException {
		this.fileName = fileName;
		quizzes = new Vector<Quiz>();
		this.validator = validator;
		loadData();
	}

	public Repository(QuizValidator validator) {
		quizzes = new Vector<Quiz>();
		this.validator = validator;
	}
	
	public void loadData() throws IOException
	{
		String line = null;
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		while ((line = br.readLine()) != null) {
			String s[] = line.split(";");
			Difficulty d = null;
			Quiz quiz = new Quiz(s[0], Integer.parseInt(s[1]), s[2], Integer.parseInt(s[3]));
			add(quiz);
		}
		br.close();
	}

	public void saveData() throws IOException {
		//BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
		//		new FileOutputStream(fileName)));
		BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));

		if (quizzes.size() != 0)
			quizzes.forEach (
				(quiz) ->{
					try {
						bw.write(quiz.getId()+";");
						bw.write(quiz.getNoQuestions()+";");
						bw.write(quiz.getDifficulty()+";");
						bw.write(quiz.getCorrectAnswers()+"");
						bw.write("\r\n");
					} catch (IOException e) {
						e.printStackTrace();
					}
                });
		bw.close();
	}

	public void add(Quiz quiz){
		List<String> errors = validator.validate(quiz);
		if (errors.size() == 0) {
			quizzes.add(quiz);
		}else System.out.println(errors);
	}

	public void save(Quiz quiz){
		add(quiz);
		try {
			saveData();
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	public List<Quiz> getAll(){
		return new ArrayList<>(quizzes);
	}
	
}
