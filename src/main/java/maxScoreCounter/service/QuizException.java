package maxScoreCounter.service;

public class QuizException extends Exception {
    public QuizException(String message) {
        super(message);
    }
}
