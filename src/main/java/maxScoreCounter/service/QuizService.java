package maxScoreCounter.service;

import java.util.ArrayList;
import java.util.List;


import maxScoreCounter.domain.Quiz;
import maxScoreCounter.repository.Repository;


public class QuizService {

		/*
		* <?- <dependency>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-surefire-plugin</artifactId>
      <version>2.22.1</version>
  </dependency>-->


  <dependency>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-failsafe-plugin</artifactId>
          <version>2.22.1</version>
      </dependency>*/
	private Repository repository;

	public QuizService(Repository repository) {
		this.repository = repository;
	}

	public List<Quiz> allQuizzes() {
		return repository.getAll();
	}

    public int maxScore(){
    	int maxScore=0;
    	List<Quiz> quizList = allQuizzes();
		
		for (int i = 0; i < quizList.size(); i++)
			if (quizList.get(i).getCorrectAnswers() > maxScore)
				maxScore = quizList.get(i).getCorrectAnswers();
		
    	return maxScore;
    }

	public List<Quiz> maxScoreQuizList() {
		List<Quiz> quizList = allQuizzes();
		List<Quiz> maxScoreQuizList = new ArrayList<Quiz>();
		int maxScore = maxScore();
		
		for (int i = 0; i < quizList.size(); i++)
			if (quizList.get(i).getCorrectAnswers() == maxScore)
				maxScoreQuizList.add(quizList.get(i));
		
		return maxScoreQuizList;
	}

	//BBT EP + BVA
	//WBT sc, dc, cc, dcc, mcc, apc, lc
	//input: the list of all quizzes
	//output: the number of quizzes that have the maximum no. of correct answers

	/**
	 * Determina numarul de quiz-uri din lista pentru care s-a obtinut punctajul maxim.
	 * Punctajul unui quiz este curprins intre 0 si 30. Lista de quiz-uri poate sa contina pana la 100 quiz-uri.
	 *
	 * @return numarul de quiz-uri pentru care s-a studentul a obtinut cel mai mare punctaj al sau.
	 *         Daca nu s-au efectuat quiz-uri atunci se returneaza 0.
	 *         Daca punctajul pentru fiecare quiz este 0 atunci se returneaza 0.
	 * @throws QuizException - daca datele de intrare nu sunt valide
	 */

	public int maxScoreQuizCounter() throws QuizException{
		List<Quiz> quizList = allQuizzes();//preia din maxScoreCounter.repository lista de quiz-uri

		int index = 0, countMax = 0, posMax = 0;

        if(quizList == null)
            throw new QuizException("null list");
        if (quizList.size()==0)
            return 0;
        if (!repository.getValidator().validate(quizList))
            throw new QuizException("data not valid");

		while (index<quizList.size()){
			if (quizList.get(index).getCorrectAnswers() > quizList.get(posMax).getCorrectAnswers()){
				posMax=index;
				countMax=1;
			}
			else
				if (quizList.get(posMax).getCorrectAnswers() == quizList.get(index).getCorrectAnswers())
					countMax++;
			index++;
		}

		if (quizList.get(posMax).getCorrectAnswers() == 0)
		    return 0;
		else return countMax;
	}	
	
	public void addQuiz(Quiz quiz) {
		
		//maxScoreCounter.repository.save(quiz);
		repository.add(quiz);
		
	}

}
