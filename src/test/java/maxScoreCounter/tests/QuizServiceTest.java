package maxScoreCounter.tests;

import maxScoreCounter.domain.Quiz;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import maxScoreCounter.repository.Repository;
import maxScoreCounter.service.QuizException;
import maxScoreCounter.service.QuizService;
import maxScoreCounter.validation.QuizValidator;

import static org.junit.Assert.*;

public class QuizServiceTest {

    private QuizService service;

    @Before
    public void setUp(){
        QuizValidator validator= new QuizValidator();
        Repository repo = new Repository(validator);
        service = new QuizService(repo);
    }

    //EP
    @Test
    public void test_valid_maxScoreQuizCounter() throws QuizException {
        service.addQuiz(new Quiz("q1", 10, "Easy", 7));
        service.addQuiz(new Quiz("q2", 15, "Medium", 3));
        service.addQuiz(new Quiz("q3", 9, "Easy", 9));
        service.addQuiz(new Quiz("q4", 5, "Hard", 2));
        service.addQuiz(new Quiz("q5", 20, "Medium", 9));

        assertEquals(2, service.maxScoreQuizCounter());
    }

@Ignore
    //BVA
    @Test
    public void test_nonvalid_MaxScoreQuizCounter() throws QuizException {
        assertEquals(-1, service.maxScoreQuizCounter());
    }



@Ignore
    //path coverage
    @Test
    public void testWBT_valid_maxScoreQuizCounter() throws QuizException {
        service.addQuiz(new Quiz("q1", 10, "Easy", 0));

        assertEquals(1, service.maxScoreQuizCounter());
    }


    //path coverage
    @Test
    public void testWBT_invalid_maxScoreQuizCounter() throws QuizException {
        //maxScoreCounter.service.addQuiz(new Quiz("q1", 10, "Easy", 0));

        assertEquals("Expected value is -1 instead of  " +service.maxScoreQuizCounter(), 0, service.maxScoreQuizCounter());
    }
    @After
    public void tearDown(){
        service = null;
    }
}