package maxScoreCounter.tests;

import maxScoreCounter.domain.Quiz;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import maxScoreCounter.repository.Repository;
import maxScoreCounter.validation.QuizValidator;

import java.io.IOException;

import static org.junit.Assert.*;

public class RepositoryTest {

    private Repository repo;

    @Before
    public void setUp(){
        QuizValidator validator= new QuizValidator();
        try {
            repo = new Repository("data/quizzes.txt", validator);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void add_validQuiz(){
        int counter = repo.getAll().size();
        Quiz quiz = new Quiz("quiz", 11, "Hard", 3);
        repo.save(quiz);
        assertEquals(counter+1, repo.getAll().size());
    }


    @Test
    public void add_invalidQuiz()  {
        int counter = repo.getAll().size();
        Quiz quiz = new Quiz("new_quiz", 0, "Hard", 3);
        repo.save(quiz);
        assertEquals(counter, repo.getAll().size());
    }

    @Ignore
    @Test
    public void add_invalidQuiz1()  {

    }

    @After
    public void tearDown(){
        repo = null;
    }

}